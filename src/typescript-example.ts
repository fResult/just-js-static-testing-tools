const username = 'freddy'
typeof username === 'string'

if (!('serviceWorker' in navigator)) {
  // you have an old browser :-(
}

const greeting = 'hello'
console.log(`${greeting} world!`)
;[1, 2, 3].forEach((x) => console.log(x))

interface IUser {
  name: {
    first: string
    middle?: string
    last: string
  }
}

function add(x: number, y: number): number {
  return x + y
}

function getFullName(user: IUser): string {
  const {
    name: { first, middle, last },
  } = user

  return [first, middle, last].filter(Boolean).join(' ')
}

getFullName({ name: { first: 'fResult', middle: 'Korn', last: 'Zilla' } })

add(1, 2)
