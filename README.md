<h1 align="center">
  <a href="https://testingjavascript.com/courses/static-analysis-testing-javascript-applications-71c1">Static Testing Tools</a>
</h1>

<div align="center">
  <h2><a href="https://testingjavascript.com">TestingJavaScript.com</a></h2>
  <a href="https://testingjavascript.com">
    <img
      width="500"
      alt="Learn the smart, efficient way to test any JavaScript application."
      src="https://kentcdodds.com/images/testingjavascript-promo/tjs-4.jpg"
    />
  </a>
</div>

<h2 align="center">
  All Testing JS
</h2>

1. [Fundamentals of Testing in JavaScript](https://gitlab.com/fResult/just-testing-javascript)
2. [JavaScript Mocking Fundamentals](https://gitlab.com/fResult/just-mocking-js-fundamentals)
3. [Static Analysis Testing JavaScript Applications](https://gitlab.com/fResult/just-js-static-testing-tools) << You are here
4. [Use DOM Testing Library to test any JS framework](https://gitlab.com/fResult/just-dom-testing-library-with-anything)
5. [Configure Jest for Testing JavaScript Applications](https://gitlab.com/fResult/just-jest-cypress-react-babel-webpack)
6. [Test React Components with Jest and React Testing Library](https://gitlab.com/fResult/just-react-testing-library-course)
7. [Install, Configure, and Script Cypress for JavaScript Web Applications](#)
8. [Test Node.js Backends](#)
